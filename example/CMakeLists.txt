##############################################################################
#
#  $Id$
#
#  Copyright 2019 Richard Hacker (lerichi at gmx dot net)
#
#  This file is part of the pdserv library.
#
#  The pdserv library is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or (at your
#  option) any later version.
#
#  The pdserv library is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
#  License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

INCLUDE_DIRECTORIES(
    ${PROJECT_BINARY_DIR}
    ${PROJECT_BINARY_DIR}/include
    )

LINK_DIRECTORIES(${pdserv_BINARY_DIR}/src)

ADD_EXECUTABLE(pdserv-example-st example-st.c)
TARGET_LINK_LIBRARIES(pdserv-example-st ${PROJECT_NAME})

INSTALL (TARGETS pdserv-example-st
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
